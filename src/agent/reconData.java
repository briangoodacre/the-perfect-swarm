package agent;

// hello
import java.util.Date;

public class reconData {
	
	int [] areaRecon = new int[ 9 ];
	Date [] reconTime = new Date[ 9 ];
	
	public reconData( int [] areaRecon, Date [] reconTime ) {
		
		this.areaRecon = areaRecon;
		this.reconTime = reconTime;
	}

	public int[] getAreaRecon() {
		return areaRecon;
	}

	public Date[] getReconTime() {
		return reconTime;
	}

}
