package agent;

import java.util.*;


public class agentTable {
	
	public int agentCount  = 0;
	public ArrayList< agent > allAgents;
	
	//Constructor
	public agentTable() {
		allAgents = new ArrayList < agent > ();
	}
	
	//Methods
	public void addAgent( agent addNow ) {
		addNow.ID = agentCount;
		allAgents.add( addNow );
		agentCount++;
	}

}
