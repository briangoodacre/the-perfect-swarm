package environment;

import java.lang.Integer;

import java.util.ArrayList;

import java.awt.*;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import settings.Settings;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class UI extends JFrame {
	
	JPanel panel = new JPanel();
	
	int side;
	int cellCount;
	
	ArrayList<JPanel> reference;
	
	String currentfinal, currentbase, base, folder10, folder20, folder25, folder30, folder35, grass, bridge, water, road, water_boulder, grass_boulder, tower, red;
	
	public UI() {
		
		base = "D:/workspace/icons";
		folder10 = "/side10";
		folder20 = "/side20";
		folder25 = "/side25";
		folder30 = "/side30";
		folder35 = "/side35";
		grass = "/grass.png";
		bridge = "/bridge.png";
		water = "/water.png";
		road = "/road.png";
		water_boulder = "/water_boulder.png";
		grass_boulder = "/grass_boulder.png";
		tower = "/tower.png";
		red = "/redsquare.png";
		
		
		this.side = Settings.side;
		
		cellCount = side * side;
		
		reference = new ArrayList<JPanel>();
		
		switch( side ) {
		case 10:
			currentbase = base + folder10;
			break;
		case 20:
			currentbase = base + folder20;
			break;
		case 25:
			currentbase = base + folder25;
			break;
		case 30:
			currentbase = base + folder30;
			break;
		case 35:
			currentbase = base + folder35;
			break;
		default:
			break;
		}
		
		initUI();
	}
	
	public void initUI() {
		
		panel.setBackground( Color.DARK_GRAY );
		panel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
		panel.setLayout( new GridLayout( side, side, 5, 5 ) );
		
		for( int i = 0; i < cellCount; i++ ) {
			
			JPanel hatchling = new JPanel( new BorderLayout() );
			hatchling.setBackground( Color.BLACK );
			reference.add(hatchling);
			panel.add( reference.get(i) );
		}
		
		add( panel );
		
		setTitle( "Nexus" );
		setSize( 750, 750 );
		setLocationRelativeTo( null );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
	}
	
	public void mapLoad( String pathname ) throws IOException {
		
		int current, cellVal;
		current = 0;
		
		File inputWorkbook = new File( pathname );
		Workbook w;
		Cell cell;
		
		JLabel addPic;
		
		try {
			
			w = Workbook.getWorkbook( inputWorkbook );
			Sheet sheet = w.getSheet(0);
			
			for( int i = 0; i < side; i++ ) {
				for( int j = 0; j < side; j++ ) {
					
					cell = sheet.getCell(j,i);
					cellVal = Integer.parseInt(cell.getContents());
					
					switch( cellVal ) {
					case 0:
						addPic = new JLabel( new ImageIcon( currentbase + red ) );
						reference.get(current).add(addPic);
						break;
					case 1://grass
						addPic = new JLabel( new ImageIcon( currentbase + grass ) );
						reference.get(current).add(addPic);
						break;
					case 2://water
						addPic = new JLabel( new ImageIcon( currentbase + water ) );
						reference.get(current).add(addPic);
						break;
					case 3://road
						addPic = new JLabel( new ImageIcon( currentbase + road ) );
						reference.get(current).add(addPic);
						break;
					case 4://grass boulder
						addPic = new JLabel( new ImageIcon( currentbase + grass_boulder ) );
						reference.get(current).add(addPic);
						break;
					case 5://water boulder
						addPic = new JLabel( new ImageIcon( currentbase + water_boulder ) );
						reference.get(current).add(addPic);
						break;
					case 6://tower
						addPic = new JLabel( new ImageIcon( currentbase + tower ) );
						reference.get(current).add(addPic);
						break;
					default:
						panel.getComponent( current ).setBackground( Color.DARK_GRAY );
						break;
					}
					
					current++;
				}
			}
		}
		
		catch( BiffException e ) {
			
			e.printStackTrace();
		}
		panel.revalidate();
	}
	
	public void moveUpdate( int currLoc, int currLocColor, int nextLoc, int nextLocColor ) {
		
		JLabel addPic;
		
		switch( currLocColor ) {
		case 0:
			addPic = new JLabel( new ImageIcon( currentbase + red ) );
			reference.get(currLoc).removeAll();
			reference.get(currLoc).add(addPic);
			break;
		case 1:
			addPic = new JLabel( new ImageIcon( currentbase + grass ) );
			reference.get(currLoc).removeAll();
			reference.get(currLoc).add(addPic);
			break;
		case 2:
			addPic = new JLabel( new ImageIcon( currentbase + water ) );
			reference.get(currLoc).removeAll();
			reference.get(currLoc).add(addPic);
			break;
		case 3:
			addPic = new JLabel( new ImageIcon( currentbase + road ) );
			reference.get(currLoc).removeAll();
			reference.get(currLoc).add(addPic);
			break;
		case 4:
			addPic = new JLabel( new ImageIcon( currentbase + grass_boulder ) );
			reference.get(currLoc).removeAll();
			reference.get(currLoc).add(addPic);
			break;
		case 5:
			addPic = new JLabel( new ImageIcon( currentbase + water_boulder ) );
			reference.get(currLoc).removeAll();
			reference.get(currLoc).add(addPic);
			break;
		case 6:
			addPic = new JLabel( new ImageIcon( currentbase + tower ) );
			reference.get(currLoc).removeAll();
			reference.get(currLoc).add(addPic);
			break;
		default:
			panel.getComponent( currLoc ).setBackground( Color.DARK_GRAY );
			break;
		}
		
		switch( nextLocColor ) {
		case 0:
			addPic = new JLabel( new ImageIcon( currentbase + red ) );
			reference.get(nextLoc).removeAll();
			reference.get(nextLoc).add(addPic);
			break;
		case 1:
			addPic = new JLabel( new ImageIcon( currentbase + grass ) );
			reference.get(nextLoc).removeAll();
			reference.get(nextLoc).add(addPic);
			break;
		case 2:
			addPic = new JLabel( new ImageIcon( currentbase + water ) );
			reference.get(nextLoc).removeAll();
			reference.get(nextLoc).add(addPic);
			break;
		case 3:
			addPic = new JLabel( new ImageIcon( currentbase + road ) );
			reference.get(nextLoc).removeAll();
			reference.get(nextLoc).add(addPic);
			break;
		case 4:
			addPic = new JLabel( new ImageIcon( currentbase + grass_boulder ) );
			reference.get(nextLoc).removeAll();
			reference.get(nextLoc).add(addPic);
			break;
		case 5:
			addPic = new JLabel( new ImageIcon( currentbase + water_boulder ) );
			reference.get(nextLoc).removeAll();
			reference.get(nextLoc).add(addPic);
			break;
		case 6:
			addPic = new JLabel( new ImageIcon( currentbase + tower ) );
			reference.get(nextLoc).removeAll();
			reference.get(nextLoc).add(addPic);
			break;
		default:
			panel.getComponent( nextLoc ).setBackground( Color.DARK_GRAY );
			break;
		}
		
		panel.revalidate();
	}
	
	public void cellUpdate( int cellLoc, int newColor ) {
		
		JLabel addPic;

		switch( newColor ) {
		case 0:
			addPic = new JLabel( new ImageIcon( currentbase + red ) );
			reference.get(cellLoc).removeAll();
			reference.get(cellLoc).add(addPic);
			break;
		case 1:
			addPic = new JLabel( new ImageIcon( currentbase + grass ) );
			reference.get(cellLoc).removeAll();
			reference.get(cellLoc).add(addPic);
			break;
		case 2:
			addPic = new JLabel( new ImageIcon( currentbase + water ) );
			reference.get(cellLoc).removeAll();
			reference.get(cellLoc).add(addPic);
			break;
		case 3:
			addPic = new JLabel( new ImageIcon( currentbase + road ) );
			reference.get(cellLoc).removeAll();
			reference.get(cellLoc).add(addPic);
			break;
		case 4:
			addPic = new JLabel( new ImageIcon( currentbase + grass_boulder ) );
			reference.get(cellLoc).removeAll();
			reference.get(cellLoc).add(addPic);
			break;
		case 5:
			addPic = new JLabel( new ImageIcon( currentbase + water_boulder ) );
			reference.get(cellLoc).removeAll();
			reference.get(cellLoc).add(addPic);
			break;
		case 6:
			addPic = new JLabel( new ImageIcon( currentbase + tower ) );
			reference.get(cellLoc).removeAll();
			reference.get(cellLoc).add(addPic);
			break;
		default:
			panel.getComponent( cellLoc ).setBackground( Color.DARK_GRAY );
			break;
		}
		
		panel.revalidate();
	}
}
