package environment;

import java.lang.Integer;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import agent.reconData;
import settings.Settings;

import environment.UI;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class environment {
	
	int side = Settings.side;
	int cellCount = side*side;
	UI portal;
	
	String filename = "D:/workspace/Book1.xls";
	
	// create an array to check cell occupancy
	public int [][] GUI;
	public int [][] agentMap;
	public int [][] agentArray;
	int [][] tempArray;
	
	public environment() {
		this.side = Settings.side;
		
		this.GUI = new int[Settings.side][Settings.side]; //info for grass, water
		this.agentMap = new int[Settings.side][ 3 ];
		this.agentArray = new int[Settings.side][Settings.side];
		this.tempArray = new int[Settings.side][Settings.side]; //used for isGameFinished
			
		
		portal = new UI();
		portal.setVisible(true);
		
		for( int i = 0; i < Settings.side; i++ ) {
			for( int j = 0; j < 3; j++ ) {
				
				agentMap[ i ][ j ] = -1;
			}
		}
		
		for( int i = 0; i < side; i++ ) {
			for( int j = 0; j < side; j++ ) {
				
				GUI[ i ][ j ] = -1;
				agentArray[ i ][ j ] = -1;
			}
		}
		
		//createEnvironmentGrid();
		try {
			loadMapData( filename );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadMapData( String fileName ) throws IOException {
		
		File inputWorkbook = new File( fileName );
		Workbook w;
		Cell cell;
		
		int cellVal;
		
		try {
			
			w = Workbook.getWorkbook( inputWorkbook );
			Sheet sheet = w.getSheet(0);
			
			for( int i = 0; i < side; i++ ) {
				for( int j = 0; j < side; j++ ) {
					
					cell = sheet.getCell(j,i);
					cellVal = Integer.parseInt(cell.getContents());
					
					GUI[i][j] = cellVal;
				}
 			}
			
		} catch ( BiffException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		portal.mapLoad( fileName );
	}
	
	public int[] getAgentLocation( int agentID ) {
		
		int [] agentLoc = new int[ 2 ];
		
		agentLoc[ 0 ] = agentMap[ agentID ][ 1 ];
		agentLoc[ 1 ] = agentMap[ agentID ][ 2 ];
		
		return agentLoc;
	}
	
	public void executeMove( int agentID, int xVal, int yVal ) {
		
		int pastCell = agentMap[ agentID ][ 1 ] + side * agentMap[ agentID ][ 2 ];
		int futureCell = xVal + side * yVal;
		
		//TODO - Fix this so it does not put a road onto an agent
		portal.moveUpdate( pastCell, GUI[ agentMap[ agentID ][ 2 ] ][ agentMap[ agentID ][ 1 ] ], futureCell, 0);
		//portal.moveUpdate( pastCell, 3, futureCell, 0);

		
		agentArray[ agentMap[ agentID ][ 2 ] ][ agentMap[ agentID ][ 1 ] ] = -1;
		agentMap[ agentID ][ 1 ] = xVal;
		agentMap[ agentID ][ 2 ] = yVal;
		agentArray[ agentMap[ agentID ][ 2 ] ][ agentMap[ agentID ][ 1 ] ] = agentID;
		
		
	}
	
	public void changeEnvironment( int xVal, int yVal, int newColor ) {
		
		GUI[ yVal ][ xVal ] = newColor;
		
		portal.cellUpdate( xVal + side * yVal, newColor);
	}
	
	public void setAgent( int xVal, int yVal, int agentID ) {
		
		agentArray[yVal][xVal] = agentID;
		agentMap[agentID][1] = xVal;
		agentMap[agentID][2] = yVal;
		
		portal.cellUpdate(xVal + side * yVal, 0 ); 
	}
	
	public reconData reconArea( int agentID, int xVal, int yVal ) {
		
		int [] surveillance = new int[ 9 ]; //directly around itself
		Date [] reconTime = new Date[ 9 ];
		Date currentTime;
		reconData results;
		
		int counter = 0;
		
		for( int i = yVal - 1; i < yVal + 2; i++ ) {
			for( int j = xVal - 1; j < xVal + 2; j++ ) {
				
				try {
					
					surveillance[ counter ] = GUI[ i ][ j ];
					currentTime = new Date();
					reconTime[ counter ] = currentTime;
				}
				
				catch( ArrayIndexOutOfBoundsException e ) {
					
					surveillance[ counter ] = -1;
					reconTime[ counter ] = null;
				}
				
				counter++;
			}
		}
		
		results  = new reconData( surveillance, reconTime );
		return results;
	}
	
	public void createAgent( int xVal, int yVal ) {
		
		int current = 0;
		
		while( agentMap[ current ][ 0 ] != -1 ) {
			
			current++;
		}
		
		agentMap[ current ][ 0 ] = xVal;
		agentMap[ current ][ 1 ] = yVal;
	}
	
	public ArrayList < Integer > findFriends( int xLoc, int yLoc ) {
		
		ArrayList < Integer > friends = new ArrayList < Integer > ();
		
		for( int i = yLoc - 1;  i < yLoc + 2; i++ ) {
			for( int j = xLoc - 1; j < xLoc + 2; j++ ) {
				
				try{
					
					if( agentArray[ i ][ j ] != -1 && i != yLoc && j != xLoc ) {
					
						friends.add( agentArray[ i ][ j ] );
					}
				}
				catch( ArrayIndexOutOfBoundsException e ) {
					
				}
			}
		}
		
		return friends;
	}
	/*
	public static void main( String [] args ) {
		
		environment now = new environment();
	}
	*/
}