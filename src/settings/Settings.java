//Brian Goodacre
package settings;

import agent.agentTable;

public class Settings {
	//VARIABLES
	//variables - simple
	public static int numAgents = 1;
	//variables
	public static int side = 30;
	public static int cellCount = 900;
	public static int rows = side;
	public static int columns = side;
	public static int startX = 9;
	public static int startY = 9;
	public static int endX = startX;
	public static int endY = columns-1;
	//variables - complex
	public static int difficultyScale = 1;
	public static int agentFieldOfView = 1;
	//variables - percentages
	public static int percentageWater = 5;
	public static int percentageMountain = 5;
	public static int percentageUnpavedLand = 80; //grass
	public static int percentagePavedLand = 10;
	public static int percentageFire = 0;
	//more
	public static int reconVision = 1;
	public static int goalX = 0;
	public static int goalY = 0;
	//for move algorithm one
	public static int weightForDistance = 1;
	public static int weightForGrass = 1;
	public static double timeWaitMove = .5;
	//
	public static agentTable table = new agentTable();
	
}
