package driver;

import java.util.*;

import settings.Settings;

import agent.agentTable;

import environment.environment;

public class sysDriver implements Runnable {
	
	QueueMoves queuemoves;
	environment map;
	agentTable reference;
	
	public sysDriver( environment map, agentTable reference, QueueMoves qm) {
		this.queuemoves = qm;
		this.map = map;
		this.reference = reference;
	}
	
	public void queueMove( Move m )  {
		queuemoves.enqueue(m);
	}
	
	public void moveResult( int agentID, int result ) {
		
		if( result == 1 ) {
			reference.allAgents.get( agentID ).moveSuccess = 1;
		}
		else if( result == 0) {
			reference.allAgents.get( agentID ).moveSuccess = 0;
		}
	}
	
	public void run() {
		
        while( true ) {
        	if( queuemoves.isEmpty() == false) {
        		        		
               	Move temp = queuemoves.dequeue();
               	
               	if( map.agentArray[temp.getyLoc() ][ temp.getxLoc() ] == -1 ) {
               		
                               
               		map.executeMove(temp.getAgentNum(), temp.getxLoc(), temp.getyLoc() );
                    moveResult(temp.getAgentNum(),1);
                }
                        
                else {
                                
                	moveResult(temp.getAgentNum(),0);
                }
        	}
        }
	}
}
