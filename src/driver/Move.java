//Brian Goodacre
package driver;

public class Move {
	//class variables
	private int AgentNum;
	//private int AgentType; //0=agent,1=hostile //TODO
	private int xLoc;
	private int yLoc;
	private int direction; //0-up,1-right,2-down,3-left
	
	//constructor
	public Move(int AgentNum_, int direction_){
		AgentNum = AgentNum_;
		direction = direction_;
		this.xLoc = -1;
		this.yLoc = -1;
	}
	
	public Move(int AgentNum_, int xLoc, int yLoc){
		AgentNum = AgentNum_;
		this.direction = -1;
		this.xLoc = xLoc;
		this.yLoc = yLoc;
	}

	public int getAgentNum() {
		return AgentNum;
	}
	public int getxLoc() {
		return xLoc;
	}
	public int getyLoc() {
		return yLoc;
	}
	public int getDirection() {
		return direction;
	}
	
	
}
