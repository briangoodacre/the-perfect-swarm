//Brian Goodacre
/**This class acts like a queue but it uses an ArrayList abstraction
 * so it can have more smarter 'enqueues' in the future. 
 */
package driver;

//imports
import java.util.ArrayList;

public class QueueMoves {
	//class variables
	private ArrayList<Move> q;
	
	//constructor - this object is a queue of moves
	public QueueMoves(){
		q = new ArrayList<Move>();
	}
	
	//METHODS
	public boolean isEmpty(){
		if(q.size()==0)
			return true;
		else
			return false;
	}
	//enqueue
	public void enqueue(Move m){
		//this functions inserts the move at the end of the ArrayList
		q.add(m);//so simple
	}
	//dequeue
	public Move dequeue(){
		if(q.size()==0)
			return null;
		else{
			//this function saves the last time, removes it, then returns it
			int lastIndex = q.size()-1;
			Move temp = q.get(lastIndex);
			q.remove(lastIndex);
			return temp;
		}
	}
	
	public int size(){
		return q.size();
	}

	//determine the moves to execute
	public void determineMoves(){
		/*TODO*/
		//currently in the sysdriver class
		//edit the toExecute ArrayList
	}
}
