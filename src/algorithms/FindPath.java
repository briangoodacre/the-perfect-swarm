package algorithms;

import java.util.ArrayList;

public interface FindPath {
	
	public int nextMove();
	
	public ArrayList <Integer> moveList();

}
