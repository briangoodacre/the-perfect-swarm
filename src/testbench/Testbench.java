//Brian Goodacre
/**This class will have the main method that controls the execution of the program.*/

package testbench;

//imports
import agent.agentTable;
import settings.Settings;
import environment.environment;
import driver.QueueMoves;
import driver.sysDriver;
import agent.agent;

public class Testbench {
	
	environment environy;
	QueueMoves queuey;
	agentTable tabley;
	Runnable drivey;
	Runnable agenty;
	
	public Testbench() {
		
		environy = new environment();
		queuey = new QueueMoves();
		tabley = Settings.table;
		
		drivey = new sysDriver( environy, tabley, queuey );
		Thread node = new Thread( drivey );
		
		agenty = new agent( 0, environy, (sysDriver) drivey, 23, 29 );
		tabley.addAgent( (agent) agenty );
		Thread node0 = new Thread( tabley.allAgents.get( 0 ) );
		
		agenty = new agent( 1, environy, (sysDriver) drivey, 8, 29 );
		tabley.addAgent( (agent) agenty );
		Thread node1 = new Thread( tabley.allAgents.get( 1 ) );
		
		agenty = new agent( 2, environy, (sysDriver) drivey, 4, 27 );
		tabley.addAgent( (agent) agenty );
		Thread node2 = new Thread( tabley.allAgents.get( 2 ) );
			
		//run until game is finished
			node.start();
			node0.start();
			node1.start();
			node2.start();
	}
}